import { configureStore } from '@reduxjs/toolkit'
import productsReducer from './reducers/productsReducer'
import { modalReducer } from './reducers/modalReducer'

const store = configureStore({reducer: {products: productsReducer, modal: modalReducer}})

export default store 