export const getProducts = (store) => {
    return store.products.products
  }
export const getFavorite = (store) => {
    return store.products.favorite
  }
  export const getBasket = (store) => {
    return store.products.basket
  }
  export const getCurrentArtikul = (store) => {
    return store.modal.currentArtikul
  }
  export const getChekoutForm = (store) => {
    return store.modal.chekoutForm
  }