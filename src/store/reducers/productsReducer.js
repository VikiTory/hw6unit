import { GET_ALL_PRODUCTS, ADD_TO_BASKET, DELETE_FROM_BASKET, TOGAL_FAVORITE, RESET_BASKET} from "../actions/productsActions";

const initialBasket = localStorage.getItem("basket")?JSON.parse(localStorage.getItem("basket")):[];
const initialFavorite = localStorage.getItem("favorite")?JSON.parse(localStorage.getItem("favorite")):[];
const InitialState = {
    products: [],
    basket: initialBasket,
    favorite: initialFavorite,
}

export function productsReducer (state=InitialState, action) {
    switch(action.type) {
       case GET_ALL_PRODUCTS:{return {...state, products: action.payload}}
       case ADD_TO_BASKET: 
        {const {basket} = state 
            const isBasket = basket.includes(action.payload) 
            let newBasket = basket
        if (!isBasket) {
           newBasket = [...basket,action.payload]
        }return {...state, basket: newBasket}}
        case DELETE_FROM_BASKET: 
        {const {basket} = state 
        return {...state, basket: basket.filter((item) => 
            item !== action.payload
        )} 
        }
        case TOGAL_FAVORITE: 
        {const {favorite} = state 
        const isFavorite = favorite.includes(action.payload)
        let newFavorite = null;
          if (isFavorite) {newFavorite = favorite.filter((favId) => 
            favId!==action.payload)
          }else { 
          newFavorite = [...favorite,action.payload]
          }
          return {...state, favorite: newFavorite}
        }
        case RESET_BASKET:{
          return {...state, basket: []}
        }
          
        

       default:return state
    }


} 

export default productsReducer