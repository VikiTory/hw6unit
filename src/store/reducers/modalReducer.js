import { DELETE_ARTIKUL, SET_ARTIKUL, SET_CHEKOUT_FORM} from "../actions/modalActions"

const InitialState = {
    currentArtikul: null,
    chekoutForm: false,
}
export function modalReducer (state=InitialState, action) {
    switch(action.type) {
        case SET_ARTIKUL: {return {...state, currentArtikul: action.payload}}
        case DELETE_ARTIKUL: {return {...state, currentArtikul: null}}
        case SET_CHEKOUT_FORM: {return {...state, chekoutForm: !state.chekoutForm}}
        default: return state 
        

    }
}


