export const SET_ARTIKUL = "SET_ARTIKUL"
export const DELETE_ARTIKUL = "DELETE_ARTIKUL"
export const SET_CHEKOUT_FORM = "SET_CHEKOUT_FORM"


export const setArtikuls = (articul) => {
  return {type: SET_ARTIKUL, payload: articul}
}
export const deleteArtikul = () => {
    return {type: DELETE_ARTIKUL}
}
export const setChekoutForm = () => {
  return {type: SET_CHEKOUT_FORM}
}
