export const GET_ALL_PRODUCTS = "GET_ALL_PRODUCTS"
export const ADD_TO_BASKET = "ADD_TO_BASKET"
export const DELETE_FROM_BASKET = "DELETE_FROM_BASKET"
export const TOGAL_FAVORITE = "TOGAL_FAVORITE"
export const RESET_BASKET = "RESET_BASKET"
const fetchDataSuccess = (data) => {
    return {type:GET_ALL_PRODUCTS, payload:data}
}
export const addToBaskets = (articul) => {
 return {type:ADD_TO_BASKET, payload:articul}
}
export const deleteFromBaskets = (articul) => {
  return {type:DELETE_FROM_BASKET, payload: articul}
}
export const resetBasket = () => {
  return {type: RESET_BASKET}
}
export const fetchData = () => {
    return (dispatch) => {
       fetch("./product.json")
      .then((response) => 
       response.json()
      ).then((data) => {
        dispatch(fetchDataSuccess(data))
      })
    }
}
export const togalFavorites = (articul) => {
  return {type:TOGAL_FAVORITE, payload: articul}
}
