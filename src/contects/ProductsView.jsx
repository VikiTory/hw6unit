import React, {createContext, useState} from "react";

const ProductsViewContext = createContext();
const defaultValue = {view : 'list'};
export const ProductsViewProvider = ({children, value}) => {
    const [currentValue, setCurrentValue] = useState (value || defaultValue);
    const saveValue = (value) => {
        setCurrentValue(value)
    }
    return <ProductsViewContext.Provider value={{value : currentValue, saveValue}}>{children}</ProductsViewContext.Provider>

}
export const ProductsViewConsumer = ProductsViewContext.Consumer
export default ProductsViewContext

