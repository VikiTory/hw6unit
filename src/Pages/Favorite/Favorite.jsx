import ProductList from "../../components/ProductList/ProductList";
import { getFavorite, getProducts } from "../../store/selectors/productsSelectors";
import { useSelector } from "react-redux";
import CheckProducts from "../../components/CheckProducts/CheckProducts";

const Favorite =() => {
    const products = useSelector(getProducts);
    const favorite = useSelector(getFavorite);
    const favoriteProducts =
        products.filter((item)=> {
            if(favorite.includes(item.articul)){
                return true 
            }else{
                return false 
            }

        })
    
    return (
        <CheckProducts arrProducts={favorite}>  
            <ProductList products={favoriteProducts}/>
        </CheckProducts>
    )
    

}
export default Favorite