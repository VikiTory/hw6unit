import ModalImage from '../../components/ModalImag';
import ProductList from '../../components/ProductList/ProductList';
import { useOutletContext } from 'react-router-dom';
import TextElement from '../../components/TextElement/TextElement';
import { getUrl } from '../../helpers/getUrl';
import { useDispatch, useSelector } from 'react-redux';
import { deleteFromBaskets } from '../../store/actions/productsActions';
import { getBasket, getChekoutForm, getCurrentArtikul, getProducts } from '../../store/selectors/productsSelectors';
import ChekoutOrder from '../../components/ChekoutOrder/ChekoutOrder';
import Button from '../../components/Button/Button';
import { deleteArtikul, setChekoutForm } from '../../store/actions/modalActions';
import styles from './Basket.module.scss';
import CheckProducts from '../../components/CheckProducts/CheckProducts';
const Basket =() => {
    const chekoutOrder = useSelector(getChekoutForm);
    const basket = useSelector(getBasket)
    const products = useSelector(getProducts)
    const currentArtikul = useSelector(getCurrentArtikul)
    const dispatch = useDispatch();
    const basketProducts = 
        products.filter((item)=> {
            if(basket.includes(item.articul)){
                return true 
            }else{
                return false 
            }

        })
        const currentImg = getUrl(currentArtikul, basketProducts)
    return (
        <CheckProducts arrProducts={basket}>
        <div className={styles.wrapper} > <Button className="buyProducts" text="Buy products" onClick={() => {dispatch(setChekoutForm())}}/>
         <ProductList products={basketProducts}/>
        {currentArtikul && (
          <ModalImage
          
          closeButton={true}
          text={<>
          <TextElement type='h2' marginB='32px'>Product Delete?!</TextElement>
          <TextElement marginB='64px'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</TextElement>
          </>}
          closeModal = {() => {dispatch(deleteArtikul())}}
          footerProps={{firstText:"NO, CANCEL", secondaryText:"YES, Delete from Basket", firstClick:() => {dispatch(deleteArtikul())}, secondaryClick:()=> {dispatch(deleteFromBaskets(currentArtikul)); dispatch(deleteArtikul())}, firstClass:'cancel', firstBackground:'#8A33FD'}}
          imageSrs={currentImg}
         
         />
        )}
        { chekoutOrder && (
             <ChekoutOrder  basketProducts = {basketProducts}/>
        )
        }
        </div>
        </CheckProducts>
    )
}
export default Basket