import { useSelector } from 'react-redux';
import ProductList from '../../components/ProductList/ProductList';
import { getProducts } from '../../store/selectors/productsSelectors';

const Home =() => {
    const products = useSelector(getProducts) 
    
    return (<>  <ProductList products={products}/>
        </>
)
}       
export default Home