import { useEffect } from 'react';
import './App.css';
import Header from '../../components/header/Header';
import { Outlet, useLocation } from 'react-router-dom';
import ModalImage from '../../components/ModalImag';
import ModalText from '../../components/ModalText';
import TextElement from '../../components/TextElement/TextElement';
import { getUrl } from '../../helpers/getUrl';
import { useDispatch, useSelector }  from 'react-redux';
import { addToBaskets, fetchData} from '../../store/actions/productsActions';
import { deleteArtikul} from '../../store/actions/modalActions';
import { getBasket, getCurrentArtikul, getFavorite, getProducts } from '../../store/selectors/productsSelectors';


const Layout = () => {
    let location = useLocation();
    const isLocationBasket = location.pathname === "/basket"
    const basket = useSelector(getBasket)
    const products = useSelector(getProducts) 
    const favorite = useSelector(getFavorite)
    const currentArtikul = useSelector(getCurrentArtikul)
    const isBasket = basket?.includes(currentArtikul); 
    const dispatch = useDispatch()
    
 
  
    
    useEffect(() => {
      dispatch(fetchData())
    }, []);
    useEffect(()=> {
      localStorage.setItem("favorite", JSON.stringify(favorite))
      localStorage.setItem("basket", JSON.stringify(basket))
  
    },[favorite,basket])
    
    const currentImg = getUrl(currentArtikul, products)

    return (
      
      
      <div className="App">
        <Header favorite={favorite} basket={basket}/>
        <Outlet/>
        {currentArtikul &&!isBasket&& (
          <ModalImage
          
          closeButton={true}
          text={<>
          <TextElement type='h2' marginB='32px'>Product Add!</TextElement>
          <TextElement marginB='64px'>By clicking the “Yes, Add” button, PRODUCT NAME will be added.</TextElement>
          </>}
          closeModal = {() => {dispatch(deleteArtikul())}}
          footerProps={{firstText:"NO, CANCEL", secondaryText:"YES, Add To Basket", firstClick:()=> {dispatch(deleteArtikul())}, secondaryClick:()=> {dispatch(addToBaskets(currentArtikul));dispatch(deleteArtikul())}, firstClass:'cancel', firstBackground:'#8A33FD'}} 
          imageSrs={currentImg}
         
         />
        )}
        
        {isBasket &&!isLocationBasket&& (
     <ModalText
    
    closeButton={true}
    text={<>
       
     <TextElement marginB='73px'>Product Added To Basket</TextElement>
     </>}
     closeModal = {() => {dispatch(deleteArtikul())}}
     footerProps={{firstText:"OK, CLOSE", firstClick:()=> {dispatch(deleteArtikul())}, firstBackground:'#8A33FD', firstClass:'cancelWithMargin'}}
    />
   )}
  
  
  
      </div>
    );
  };
  
  
 
  export default Layout

