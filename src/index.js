import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import './App.scss'
import reportWebVitals from './reportWebVitals';
import { RouterProvider } from 'react-router-dom';
import router from './Routers/Routers';
import { Provider } from 'react-redux';
import store from './store/store';
import { ProductsViewProvider } from './contects/ProductsView';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ProductsViewProvider>
    <Provider store={store}>
    <RouterProvider router={router}/>
    </Provider>
    </ProductsViewProvider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
