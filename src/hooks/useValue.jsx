import { useContext } from "react"
import ProductsViewContext from "../contects/ProductsView"

export default () => {
    const context = useContext(ProductsViewContext) 
    return context
}