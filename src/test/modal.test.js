import { fireEvent, getAllByTestId, render } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import ModalImage from "../components/ModalImag";

describe ("modal.test", () => {
    it ("test stop propagation function", () => {
      const click = jest.fn()
      const {getByRole} = render (<ModalImage/>)
      const getModalBox = getByRole("modalBox")
      
      getModalBox.onclick = click
      fireEvent.click(getModalBox)
      expect (click).toHaveBeenCalledTimes(1)
    })
    it ("test modal snapshot", () => {
        const Modal = render (<ModalImage text="test"/>) 
        expect (Modal).toMatchSnapshot()
     })
     it ("text content", () => {
        const {getByTestId} = render (<ModalImage text="test"/>)
        expect (getByTestId("textContent")).toHaveTextContent("test")
     })
     it ("click close content", () => {
        const click = jest.fn()
        const {getByRole} = render (<ModalImage closeModal={click}/>)
        fireEvent.click(getByRole("modalWrapper"))
        expect (click).toHaveBeenCalledTimes(1)
     }
        
     )


})