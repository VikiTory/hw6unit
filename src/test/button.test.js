import { fireEvent, render } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import Button from "../components/Button/Button";

describe("button test", () => {
    it ("test button props", () => {
       const {getByText} = render (<Button text="test"/>) 
       const button = getByText(/test/i) 
       expect (button).toBeInTheDocument()
    })
    it ("background style test", () => {
       const {getByRole} = render (<Button backgroundColor="red"/>);
       const button = getByRole ("button")
       expect (button).toHaveStyle("background-color : red")
   })
   it ("click button function", () => {
      const click = jest.fn()
      const {getByRole} = render (<Button onClick={click}/>)
      fireEvent.click(getByRole("button"))
      expect (click).toHaveBeenCalledTimes(1)
   })
   it ("test button snapshot", () => {
      const {getByText} = render (<Button text="test"/>) 
      const button = getByText(/test/i) 
      expect (button).toMatchSnapshot()
   })
})