import { configureStore } from '@reduxjs/toolkit';
import productsReducer from '../store/reducers/productsReducer';
import { modalReducer } from '../store/reducers/modalReducer';
import { addToBaskets, deleteFromBaskets } from '../store/actions/productsActions';

describe ("my reducer", () => {
    let store;
    beforeEach(() => {
        store = configureStore({reducer: {products: productsReducer, modal: modalReducer}})
    })
    it ("add to basket", () => {
        store.dispatch(addToBaskets("#202"))
        expect (store.getState().products).toStrictEqual({basket:["#202"], products:[], favorite:[]})
    })
    it ("add to basket include articul", () => {
        store.dispatch(addToBaskets("#202"))
        store.dispatch(addToBaskets("#202"))
        expect (store.getState().products).toStrictEqual({basket:["#202"], products:[], favorite:[]})
    })
    it ("delete from basket", () => {
        store.dispatch(addToBaskets("#202"))
        store.dispatch(deleteFromBaskets("#202"))
        expect (store.getState().products).toStrictEqual({basket:[], products:[], favorite:[]})
    })
})