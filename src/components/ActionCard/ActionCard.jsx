import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import Button from "../Button/Button"
import { setArtikuls } from "../../store/actions/modalActions";

const ActionCard = ({articul}) => {
    let location = useLocation();
    const isBasket = location.pathname === "/basket"
    const dispatch = useDispatch();
    return (
    <>
    {isBasket ?<Button
    backgroundColor="blue"
    text="Delete from card"
    onClick={() => {dispatch(setArtikuls(articul))
    }} />
    : <Button
    backgroundColor="blue"
    text="Add to card"
    onClick={() => {
      dispatch(setArtikuls(articul))
    }} />}
    </>

    )
}

export default ActionCard