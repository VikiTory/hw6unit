
import Modal from "./modal/Modal";
import ModalBody from "./modal/ModalBody";
import ModalClose from "./modal/ModalClose";
import ModalFooter from "./modal/ModalFooter";
import ModalHeader from "./modal/ModalHeader";
import ModalWrapper from "./modal/ModalWrapper";
import PropTypes from 'prop-types';

const ModalImage = ({closeButton, text, closeModal, footerProps, imageSrs}) => {
  return (
    <ModalWrapper click={closeModal}>
      <Modal click = {(e) => {
        e.stopPropagation()
    }}>
        <ModalHeader>
        {closeButton && <ModalClose  click={closeModal}/>}
        </ModalHeader>
        <ModalBody><><img style={{marginBottom:"85px", maxWidth:"100%"}} src={imageSrs} alt="img" />{text}</></ModalBody>
        <ModalFooter {...footerProps}></ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};
ModalImage.propTypes = {
  closeButton: PropTypes.bool,
  text: PropTypes.object,
  closeModal: PropTypes.func,
  footerProps: PropTypes.object,
  imageSrs: PropTypes.string,
};
export default ModalImage
