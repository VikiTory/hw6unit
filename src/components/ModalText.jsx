import Modal from "./modal/Modal";
import ModalBody from "./modal/ModalBody";
import ModalClose from "./modal/ModalClose";
import ModalFooter from "./modal/ModalFooter";
import ModalHeader from "./modal/ModalHeader";
import ModalWrapper from "./modal/ModalWrapper";
import PropTypes from 'prop-types';

const ModalText = ({closeButton, text, closeModal, footerProps}) => {
    console.log(closeModal)
  return (
    <ModalWrapper click={(e) => {
        e.stopPropagation()
        closeModal()
    }} >
      <Modal>
        <ModalHeader>
        {closeButton && <ModalClose click={closeModal}/>}
        </ModalHeader>
        <ModalBody>{text}</ModalBody>
        <ModalFooter {...footerProps}></ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};
ModalText.propTypes = {
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  closeModal: PropTypes.func,
  footerProps: PropTypes.object,
};
export default ModalText
