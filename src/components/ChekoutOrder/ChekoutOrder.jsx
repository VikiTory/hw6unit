import React from 'react';
import { useFormik } from 'formik';
import * as Yup from "yup";
import styles from "./ChekoutOrder.module.css"
import { setChekoutForm } from '../../store/actions/modalActions';
import { useDispatch } from 'react-redux';
import Button from '../Button/Button';
import { resetBasket } from '../../store/actions/productsActions';


const ChekoutOrder = ({basketProducts}) => {
   
  
  const validationSchema = Yup.object().shape({
    fullName: Yup.string().required('Fullname is required')
      .min(6, 'Username must be at least 6 characters')
      .max(20, 'Username must not exceed 20 characters'),
    name: Yup.string()
      .required('Username is required')
      .min(6, 'Username must be at least 6 characters')
      .max(20, 'Username must not exceed 20 characters'),
    age: Yup.number().required('Age is required').positive('Age is positive').integer('Age is integer')
    .moreThan(17, 'min value'),
    adress: Yup.string().required('Adress is required')
    .min(5, 'Username must be at least 5 characters'),
    mobile: Yup.string().matches(/^\+?3?8?(0\d{9})$/, 'Phone number is not valid'),
  }); 
  const dispatch = useDispatch();
  
  const formik = useFormik({
    initialValues: {
      name: '',
      fullName: '',
      age: '',
      adress: '',
      mobile: '',
    },
    validationSchema,
    onSubmit: (values) => { 
      console.log('userInfo', values);
      console.log('Products in order', basketProducts);
      dispatch(resetBasket())
      dispatch(setChekoutForm())
    },
  });
  //console.log('form values', formik.values)
  return (
    <div className={styles.formWrapper} onClick={(e) => {dispatch(setChekoutForm())}}>
      <form className={styles.form} onSubmit={formik.handleSubmit} onClick={(e) => {e.stopPropagation()}}>
        <label>Name</label>
        
        <input
          type="text"
          name="name"
          id="name"
          value={formik.values.name}
          onChange={formik.handleChange}
        />
        {formik.errors.name ? (
          <div className={styles.error}>{formik.errors.name}</div>
        ) : null}
         <label>Full name</label>
        
        <input
          type="text"
          name="fullName"
          id="fullName"
          value={formik.values.fullName}
          onChange={formik.handleChange}
        />
        {formik.errors.fullName ? (
          <div className={styles.error}>{formik.errors.fullName}</div>
        ) : null}
        <label>Age</label>
        
        <input
          type="number"
          name="age"
          id="age"
          value={formik.values.age}
          onChange={formik.handleChange}
        />
        {formik.errors.age? (
          <div className={styles.error}>{formik.errors.age}</div>
        ) : null}
        <label>Adress</label>
        
        <input
          type="text"
          name="adress"
          id="adress"
          value={formik.values.adress}
          onChange={formik.handleChange}
        />
        {formik.errors.adress? (
          <div className={styles.error}>{formik.errors.adress}</div>
        ) : null}
        <label>Mobile format:+380XXXXXXXXX</label>
        
        <input
          type="text"
          name="mobile"
          id="mobile"
          value={formik.values.mobile}
          onChange={formik.handleChange}
        />
        {formik.errors.mobile? (
          <div className={styles.error}>{formik.errors.mobile}</div>
        ) : null}
        <div className={styles.buttonWrapper}>
        <Button className={styles.button} type="submit" text="Buy" backgroundColor="blue"/>
        <Button className={styles.button} onClick={(e) => {dispatch(setChekoutForm())}} text="Close" backgroundColor="yellow"/>
        </div>
      </form>
    </div>
  );
};

export default ChekoutOrder
