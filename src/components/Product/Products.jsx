import styles from "./Product.module.scss"
import PropTypes from 'prop-types';
import FavoriteIcon from "../FavoriteIcon/FavoriteIcon";
import ActionCard from "../ActionCard/ActionCard";

const Product = ({product}) => {
    return (
        <li className={styles.item} ><h3>{product.name}</h3>
        <FavoriteIcon articul={product.articul} addClass={styles.svg}/>
          <img className={styles.img} src={product.url} alt={product.name}/>
          <div>
          <h3>{product.price}</h3>
          <h3>{product.articul}</h3>
          <h3>{product.color}</h3>
        <ActionCard articul={product.articul}/>  
      </div>
      </li>
    )
}
Product.propTypes = {
  product: PropTypes.object,
  isFavorite: PropTypes.bool,
};
export default Product