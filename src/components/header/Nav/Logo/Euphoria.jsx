const Euphoria =  ({ title}) => {
    return <span className="logo_euphoria">{title}</span>;
};
Euphoria.propTypes= {
    title: PropTypes.string,
    className: PropTypes.string,
};
export default Euphoria;