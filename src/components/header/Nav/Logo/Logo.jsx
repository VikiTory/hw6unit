const Logo = () => {
    return (
        <a href="#" className="nav_logo logo">
            <Euphoria></Euphoria>
            <Group></Group>
        </a>
    );
};
Logo.propTypes = {
    className: PropTypes.string,
    href: PropTypes.string,
}; 
export default Logo;