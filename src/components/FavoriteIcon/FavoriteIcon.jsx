import { useLocation } from "react-router-dom";
import {ReactComponent as FavIcon} from "../../assets/favorite.svg"
import { useDispatch, useSelector } from "react-redux";
import { togalFavorites } from "../../store/actions/productsActions";
import { getFavorite } from "../../store/selectors/productsSelectors";


const FavoriteIcon = ({articul, addClass}) => {
    const favorite = useSelector(getFavorite);
    const dispatch = useDispatch();
    let location = useLocation();
    const isBasket = location.pathname === "/basket";
    const isFavorite=favorite.includes(articul);
    const color = isFavorite?"gold":"white" 
    return (
    <>
    {!isBasket && <FavIcon onClick={() => {dispatch(togalFavorites(articul))}} className={addClass} style={{fill:color}}/>}
    </>
    )
}

export default FavoriteIcon