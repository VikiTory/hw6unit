import React, { useMemo } from "react";
import Table from "./Table.jsx";
import FavoriteIcon from "../FavoriteIcon/FavoriteIcon";
import ActionCard from "../ActionCard/ActionCard.jsx";
import styles from "./Table.module.scss"

const TableProducts = ({products}) => {
    const columns = useMemo(
        () => [
          {
            Header: "Products",
              columns: [
              {
                Header: "Name",
                accessor: "name",
              },
              {
                Header: "Price",
                accessor: "price",
              },
              {  Header: 'Product picture', Cell: tableProps  => (
                <div><img className={styles.img} src={tableProps.row.original.url} /></div>
              )},
              {
                Header: "Articul",
                accessor: "articul",
              },
              {
                Header: "Color",
                accessor: "color",
              },
              {  Header: 'Favorite', Cell: tableProps  => (
                <FavoriteIcon articul={tableProps.row.original.articul}/>
              )},
              {  Header: 'Add basket', Cell: tableProps  => (
                <ActionCard articul={tableProps.row.original.articul}/>
              )},
            ],
          },
        ],
        []
      );
    return (
        <Table columns={columns} data={products} />
    )
}

export default TableProducts