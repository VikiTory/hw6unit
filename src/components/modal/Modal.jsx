import styles from './Modal.module.css' 

const Modal = ({click, children}) => {
    return (
    <div onClick={click} role="modalBox"  className = {styles.modal}>{children}</div>
    )
}
export default Modal 
