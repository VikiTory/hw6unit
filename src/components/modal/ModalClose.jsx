import styles from './Modal.module.css' 
import {ReactComponent as CloseIcon} from "../../assets/close.svg"

const ModalClose = ({click}) => {
    return (
       
        <CloseIcon onClick={click} className= {styles.close}/>
    )
}
export default ModalClose