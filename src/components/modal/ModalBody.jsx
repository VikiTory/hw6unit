const ModalBody = ({children})=>{
    return(
        <div className="modal-content" data-testid = "textContent">{children}</div>
    )
}
export default ModalBody