const ModalWrapper = ({children, click}) => {
    return <div className='modal' role="modalWrapper" onClick={click}>{children}</div>

}

export default ModalWrapper