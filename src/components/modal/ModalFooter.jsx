import Button from "../Button/Button"
import styles from './Modal.module.css'


const ModalFooter = ({firstText, secondaryText, firstClick, secondaryClick, firstClass, secondClass,firstBackground='', secondBackground=''}) => {
    return <div className= {styles.btnWrapper}>{firstText && <Button text = {firstText} onClick={firstClick} className={firstClass} backgroundColor={firstBackground}/>}
    {secondaryText && <Button text = {secondaryText} onClick={secondaryClick} className={secondClass} backgroundColor={secondBackground}/>}
    </div>
  
}

export default ModalFooter