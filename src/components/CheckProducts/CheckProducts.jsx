import { Link } from "react-router-dom";
import styles from "./CheckProducts.module.css"
function CheckProducts ({arrProducts, children}) {
  
    return (
        <div>
        { arrProducts.length ?children:<div><h2>You don't choose any products</h2>
    <Link className={styles.link} to="/">Home</Link></div> }
        </div>
    )
}
export default CheckProducts