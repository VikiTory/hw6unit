import Product from "../Product/Products"
import PropTypes from 'prop-types';
import styles from "./ProductList.module.scss"
import { getFavorite } from "../../store/selectors/productsSelectors";
import { useDispatch, useSelector } from "react-redux";
import useValue from "../../hooks/useValue";
import Button from "../Button/Button";
import TableProducts from "../Table/TableProducts.jsx";

const ProductList = ({products}) => {
const dispatch = useDispatch();
const favorite = useSelector(getFavorite);
const {value, saveValue} = useValue();
const saveNewValue = value.view === "list" ? "tablet" : "list"
    
    return (
      <div className={styles.listWrapper}>
        <Button className ="buttonProducts" text={saveNewValue.toUpperCase()} onClick={() => {saveValue({view:saveNewValue})}}/>
        {saveNewValue === "tablet" ?  <ul className={styles.list} > 
            {products.map((product) =>{
        
        return (
          <Product product={product} key={product.articul} isFavorite={favorite.includes(product.articul)}/>
        )

      })
      }
        </ul> : 
        <TableProducts products={products}/>}
        </div>
        
    )
}
ProductList.propTypes = {
  products: PropTypes.array,
};
export default ProductList